var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

var port = process.env.PORT || 8080;

var router = express.Router();

var url = "http://localhost/kanye.gif";

router.get('/', function(req, res) {
	res.json({ 'response_type': 'in_channel', 'text': 'You\'re supposed to be using /gifs!', 'attachments': [{ 'text': 'You\'re supposed to be using /gifs!', 'image_url':url }] });
});

app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);